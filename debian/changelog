xfce4-genmon-plugin (4.1.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.1.1.
  * d/control:
    - R³: no.
    - Update 'Homepage' field, goodies.xfce → docs.xfce.

 -- Unit 193 <unit193@debian.org>  Thu, 04 Feb 2021 17:40:27 -0500

xfce4-genmon-plugin (4.1.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Name (from ./configure), Repository.
  * Update standards version to 4.4.1, no changes needed.
  * Update standards version to 4.5.0, no changes needed.

  [ Yves-Alexis Perez ]
  * d/watch: use version 4 and uscan special strings
  * New upstream version 4.1.0
  * d/control: update dh compat level to 12
  * d/control: update standards version to 4.5.1

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 23 Dec 2020 15:39:06 +0100

xfce4-genmon-plugin (4.0.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.0.2
  * d/compat, d/control:
    -Drop d/compat in favor of debhelper-compat, bump to 11.
  * d/control:
    - Drop libxml-parser-perl from Build-Depends.
    - Mark xfce4-genmon-plugin M-A: same.
  * Update Standards-Version to 4.4.0.
  * Trim whitespace from previous changelog entries.

 -- Unit 193 <unit193@ubuntu.com>  Fri, 30 Aug 2019 23:02:14 -0400

xfce4-genmon-plugin (4.0.1-2) unstable; urgency=medium

  * Moved the package to git on salsa.debian.org
  * Updated the maintainer address to debian-xfce@lists.debian.org
                                                                closes: #899735
  * d/gbp.conf added, following DEP-14
  * d/watch: use HTTPS protocol
  * d/control: drop Lionel from uploaders, thanks!
  * d/control: update standards version to 4.2.1
  * New upstream version 4.0.1
  * update debhelper compat to 10

 -- Yves-Alexis Perez <corsac@debian.org>  Mon, 10 Dec 2018 22:41:04 +0100

xfce4-genmon-plugin (4.0.1-1) unstable; urgency=medium

  [ Unit 193 ]
  * New upstream version.
  * Run wrap-and-sort.
  * d/control:
    - Update build-depends for GTK3 release.
      + libxfce4ui-1-dev → libxfce4ui-2-dev
      + xfce4-panel-dev → libxfce4panel-2.0-dev
    - Use https in homepage and vcs-browser fields.
    - Update Standards-Version to 4.1.1.

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 11 Nov 2017 14:59:33 +0100

xfce4-genmon-plugin (3.4.0-2) unstable; urgency=low

  * debian/rules:
    - enable all hardening flags.
  * debian/control:
    - drop build-dep on dpkg-dev, version is recent enough in stable to have
      hardening support.
    - drop bersion on xfce4-panel build-dep, stable already has 4.8.
    - drop Emmanuele and Simon from uploaders, thanks to them!

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 24 May 2013 08:04:23 +0200

xfce4-genmon-plugin (3.4.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - replace libxfcegui4 build-dep by libxfce4ui.
    - update debhelper build-dep to 9.
    - update standards version to 3.9.3.
  * debian/rules:
    - pass --disable-static to configure and stop removing .la files.
    - use multiarch paths.

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 13 May 2012 13:13:21 +0200

xfce4-genmon-plugin (3.3.1-1) unstable; urgency=low

  * New upstream release.
    - fix plugin not refreshing before going to properties.     closes: #623697
  * debian/rules:
    - use hardening rules from dh9 and dpkg-dev 1.16.1.
  * debian/compat bumped to 9
  * debian/control:
    - update debhelper build-dep for hardening support.
    - add dpkg-dev 1.16.1 build-dep for hardening support.

 -- Yves-Alexis Perez <corsac@debian.org>  Mon, 02 Jan 2012 22:03:26 +0100

xfce4-genmon-plugin (3.3.0-1) unstable; urgency=low

  [ Evgeni Golov ]
  * Fix Vcs-* fields, they were missing 'trunk' in the path.

  [ Yves-Alexis Perez ]
  * New upstream release.
  * debian/watch edited to track Xfce archive reorganisation.
  * debian/control:
    - switch to xfce section.
    - add build-dep on libxfcegui4-dev.
    - update standards version to 3.9.2.
    - add ${misc:Depends} to dependencies.
    - update debhelper build-dep for overrides support.
    - drop cdbs build-dep.
    - add build-dep on hardening-includes.
    - update Xfce build-deps to 4.8.
  * debian/patches:
    - 01_no-zombies dropped, included upstream.
    - 02_close-unused-pipes as well.
  * debian/rules:
    - drop simple-patchsys include.
    - pick build flags from dpkg-buildflags.
    - add -z,defs, --as-needed and -O1 to LDFLAGS.
    - add hardening flags to build flags.
    - switch to debhelper 7 tiny.rules with overrides.
  * Switch to 3.0 (quilt) source format
  * debian/compat bumped to 7.

  [ Lionel Le Folgoc ]
  * debian/control: add myself to Uploaders.

 -- Yves-Alexis Perez <corsac@debian.org>  Tue, 19 Apr 2011 23:01:02 +0200

xfce4-genmon-plugin (3.2-3) unstable; urgency=low

  * debian/patches: 02_close-unused-pipes added, don't hang on non-finishing
    processes. (Xfce #4036).
  * debian/control:
    - update standards version to 3.8.0.
    - remove Rudy Godoy, Martin Loschwitz from Uploaders.

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 18 Jun 2008 09:22:06 +0200

xfce4-genmon-plugin (3.2-2) unstable; urgency=low

  * debian/patches:
    - 01_no-zombies added: no zombies left by spawned process.  closes: #422572

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 16 Apr 2008 22:10:44 +0200

xfce4-genmon-plugin (3.2-1) unstable; urgency=low

  [ Simon Huggins ]
  * debian/control: Move fake Homepage field to a real one now dpkg
    supports it.
  * Add Vcs-* headers to debian/control

  [ Yves-Alexis Perez ]
  * New upstream release.
    - now supports ~ in command lines.                          closes: #435109
  * debian/control:
    - updated standards version to 3.7.3.
    - updated my email address.
    - removed Jani Monoses from Uploaders:.
    - updated Homepage: field.
  * debian/copyright:
    - updated download address.
    - updated copyright holders

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 16 Mar 2008 00:41:20 +0100

xfce4-genmon-plugin (3.1-1) unstable; urgency=low

  (Yves-Alexis Perez)
  * New upstream release.
  * debian/patches: remove patch to fix implicit pointer conversion, merged
    upstream.
  (Simon Huggins)
  * Build against latest panel.

 -- Yves-Alexis Perez <corsac@corsac.net>  Sun, 29 Apr 2007 14:57:21 +0100

xfce4-genmon-plugin (3.0-2) unstable; urgency=low

  (Yves-Alexis Perez)
  * added a patch to fix implicit pointer conversion.		closes: #402839
  (Stefan Ott)
  * debian/control: goodies website URL updated.

 -- Yves-Alexis Perez <corsac@corsac.net>  Fri, 05 Jan 2007 16:43:35 +0100

xfce4-genmon-plugin (3.0-1) unstable; urgency=low

  * New upstream release.
  * Incremented build-dep to 4.3.99.2 (Xfce 4.4rc2).

 -- Yves-Alexis Perez <corsac@corsac.net>  Thu, 16 Nov 2006 18:31:29 +0100

xfce4-genmon-plugin (2.0-2) unstable; urgency=low

  * Incremented build-deps to 4.3.90.2 (Xfce 4.4 Beta2).

 -- Yves-Alexis Perez <corsac@corsac.net>  Wed, 26 Jul 2006 17:48:57 +0100

xfce4-genmon-plugin (2.0-1) unstable; urgency=low

  * Initial Release.                                           closes: #309860

 -- Yves-Alexis Perez <corsac@corsac.net>  Sun, 30 Apr 2006 16:46:30 +0200
